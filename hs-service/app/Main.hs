import           Control.Monad.Logger
import Control.Monad.Trans (lift, liftIO)
import           Data.Config                 (extractConfig, readValue)

import qualified Service.Web                 as Web
import           Control.Monad.Managed


main :: IO ()
main = runStderrLoggingT $ do
  logInfoN "Reading configuration from config/dev.yaml"
  config <- readValue "config/dev.yaml"
  serverC <- lift $ extractConfig "web" config
  runManagedT $ do
    serverH <- managedT $ Web.withHandle serverC
    liftIO (Web.runServer serverH)
