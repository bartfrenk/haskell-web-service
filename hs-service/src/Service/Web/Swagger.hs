module Service.Web.Swagger where

import           Data.Proxy
import           Data.String.Conv     (toS)
import           Data.Swagger
import           Data.Version
import           Lens.Micro
import           Servant.Swagger

import qualified Service.Web.API      as API
import qualified Service.Web.Handlers as H
import qualified Service.Web.Types    as API


swagger :: Swagger
swagger = toSwagger (Proxy @API.Service)
  & info.title .~ "Service"
  & info.version .~ toS (showVersion $ API.unVersion $ API.version H.info)
  & info.description ?~ "Some API"
