module Service.Web.Types where

import           Data.Aeson       as JSON
import           Data.Proxy
import           Data.String.Conv (toS)
import           Data.Swagger     (ToSchema (..))
import           Data.Text
import qualified Data.Version     as V
import           GHC.Generics

data Version = Version { unVersion :: V.Version } deriving (Show)

instance ToJSON Version where
  toJSON (Version version) = JSON.String $ toS (V.showVersion version)
instance ToSchema Version where
  declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy Text)

data ServiceInfo = ServiceInfo
  { version :: Version
  , name    :: Text
  } deriving (Generic, Show)

instance ToJSON ServiceInfo
instance ToSchema ServiceInfo


