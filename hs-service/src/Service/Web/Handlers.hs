module Service.Web.Handlers where

import qualified Paths_hs_service  as Meta
import           Service.Web.Types

handleInfo :: Monad m => m ServiceInfo
handleInfo = do
  pure $ info

info :: ServiceInfo
info = ServiceInfo
  { version = Version Meta.version
  , name = "hs-service" }
