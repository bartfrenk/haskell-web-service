{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
module Service.Web.API
  ( Service
  ) where


import           Servant.API
import           Service.Web.Types

type Service = "service" :> "api " :> (
  Summary "identify the service"
  :> Description "List the name and version of the service"
  :> "info" :> Get '[JSON] ServiceInfo)
