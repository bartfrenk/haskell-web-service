module Service.Web
  ( Config (..),
    Handle (..),
    withHandle
  ) where

import           Control.Monad.Logger     (MonadLoggerIO(..), logInfoN, runLoggingT)
import           Control.Monad.State      (execState, modify)
import           Control.Monad.Trans      (MonadIO, liftIO)
import           Data.Aeson               (FromJSON)
import           Data.String              (fromString)
import           Data.String.Conv         (toS)
import           Data.Text                (Text)
import           GHC.Generics             (Generic)
import qualified Network.Wai.Handler.Warp as Warp
import           Servant
import           Utils.Swagger

import qualified Service.Web.API          as API
import           Service.Web.Handlers
import           Service.Web.Swagger

type API = SwaggerUI "service" :<|> API.Service

data Config = Config
  { port :: Int
  , host :: Text }
  deriving (Eq, Show, Generic)

showAddress :: Config -> Text
showAddress Config {port, host} = host <> ":" <> toS (show port)

instance FromJSON Config

newtype Handle = Handle
  { runServer :: IO () }

type MonadServer m = (MonadIO m, MonadLoggerIO m)

withHandle :: MonadServer m => Config -> (Handle -> m a) -> m a
withHandle config cont = do
  logger <- askLoggerIO
  cont Handle {
    runServer = flip runLoggingT logger $ do
        logInfoN $ "Starting server on " <> showAddress config
        liftIO $ Warp.runSettings (warpSettings config) $
          serve api (hoistServer api (`runLoggingT` logger) server)
    }
  where
    api = Proxy @API

    warpSettings Config {host, port} = flip execState Warp.defaultSettings $ do
      modify $ Warp.setPort port
      modify $ Warp.setHost (fromString $ toS host)


server :: MonadServer m => ServerT API m
server = swaggerServer swagger :<|> handleInfo
