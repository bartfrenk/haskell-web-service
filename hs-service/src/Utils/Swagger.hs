{-# LANGUAGE TypeFamilies #-}
module Utils.Swagger
  ( SwaggerUI
  , swaggerServer'
  , swaggerServer
  , toSwagger
  ) where


import           Data.ByteString                (ByteString)
import           Data.Swagger                   (Swagger)
import           Data.Text                      (Text)
import           GHC.TypeLits                   (AppendSymbol, Symbol)
import           Network.Wai.Application.Static (embeddedSettings, staticApp)
import           Servant
import           Servant.HTML.Blaze
import           Servant.Swagger                (toSwagger)
import           Servant.Swagger.UI             (swaggerUiFiles, swaggerUiIndexTemplate)
import           Servant.Swagger.UI.Core        (SwaggerUiHtml(..))


swaggerServer' :: (Monad m, ServerT (SwaggerJSON name) m ~ m Swagger)
               => Text
               -> [(FilePath, ByteString)]
               -> Swagger
               -> ServerT (SwaggerUI name) m
swaggerServer' template files swagger
  = pure swagger :<|>
    pure (SwaggerUiHtml template) :<|>
    pure (SwaggerUiHtml template) :<|>
    rest
  where
    rest = Tagged $ staticApp $ embeddedSettings files

swaggerServer :: (Monad m, ServerT (SwaggerJSON name) m ~ m Swagger)
              => Swagger -> ServerT (SwaggerUI name) m
swaggerServer = swaggerServer' swaggerUiIndexTemplate swaggerUiFiles

type SwaggerJSON (name :: Symbol) = name :> "api" :> "swagger.json" :> Get '[JSON] Swagger

type SwaggerHTML (name :: Symbol) = SwaggerUiHtml (AppendSymbol name "/api") (SwaggerJSON name)

type SwaggerUI (name :: Symbol) =
  SwaggerJSON name :<|>
  name :> "api" :> (
    Get '[HTML] (SwaggerHTML name) :<|>
    "index.html" :> Get '[HTML] (SwaggerHTML name) :<|>
    Raw)

